# README #

1) import project to java IDE (eclipse)
2) Run application from CommandLineInterface.java
3) The commands applicable can refer to the original assignment
login Bob
pay Alice 50
topup 100
4) Refer to the <username>.json file created for the data of the user

Notes
1) User names is case sensetive
2) /testcases folder contains expected data after each test step. 
Copy the json file to replace the json at root of project
if need restart the simulation from first step use /testcases/original folder

### How do I get set up? ###

install maven / git
clone the project into your local workspace
go to the project folder
mvn exec:java -Dexec.mainClass="com.bank.ui.CommandLineInterface"

### Contribution guidelines ###


### Who do I talk to? ###

darklynx7@yahoo.com