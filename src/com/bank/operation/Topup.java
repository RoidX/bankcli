package com.bank.operation;

import java.math.BigDecimal;
import java.util.List;

import com.bank.data.BankUser;
import com.bank.data.Transaction;
import com.bank.util.BankUtil;

public class Topup {
	public Topup(BankUser user, BigDecimal amount) {
//		List<BankUser> users = new ArrayList<BankUser>();
		if(amount.compareTo(BigDecimal.ZERO) < 0) {
			System.out.println("Invalid Topup Amount " + amount.toPlainString());
		}else {
			//Determine outstanding amount to resolve first
			BigDecimal remainingTopup = amount;
			List<Transaction> transactions = user.getTransactions();
			if(!transactions.isEmpty()) {
				for(int i=0;i<transactions.size();i++) {
					Transaction transaction = transactions.get(i);
					if(transaction.getTransactionAmount().compareTo(BigDecimal.ZERO) < 0) {
					BigDecimal maximumPay = remainingTopup.compareTo(transaction.getTransactionAmount().negate()) > 0 ? transaction.getTransactionAmount().negate() : remainingTopup;
					if(remainingTopup.compareTo(transaction.getTransactionAmount().negate()) > 0) {
						remainingTopup = remainingTopup.subtract(maximumPay);
						transactions.remove(i);
						removeTransaction(transaction.getRecipent(), user.getUsername()) ;
					}else {
						transaction.setTransactionAmount(transaction.getTransactionAmount().add(maximumPay));
						remainingTopup = remainingTopup.subtract(maximumPay);	
						subtractTransaction(transaction.getRecipent(), user.getUsername(), maximumPay) ;
					}
					if(remainingTopup.compareTo(BigDecimal.ZERO)==0) {
						break;
					}
					}
				}
			}
			if(remainingTopup.compareTo(BigDecimal.ZERO) > 0) {
				//No more outstanding, add remaining to the total
				user.setCurrentBalance(user.getCurrentBalance().add(remainingTopup));
			}
			
			BankUtil.writeData(user);
		}
	}
	
	public void removeTransaction(String payee, String user) {
		BankUser payeeBankUser = BankUtil.loadData(payee) ;
		List<Transaction> transactions = payeeBankUser.getTransactions();
		BigDecimal amount = BigDecimal.ZERO;
		
		if(!transactions.isEmpty()) {
			for(int x=0;x<transactions.size();x++) {
				Transaction transaction = transactions.get(x);
				if(transaction.getRecipent().equals(user)) {
					amount = transaction.getTransactionAmount();
					transactions.remove(x);
					break;
				}
			}
		}
		payeeBankUser.setCurrentBalance(payeeBankUser.getCurrentBalance().add(amount));
		BankUtil.writeData(payeeBankUser);
	}
	
	public void subtractTransaction(String payee, String user, BigDecimal amount) {
		BankUser payeeBankUser = BankUtil.loadData(payee) ;
		List<Transaction> transactions = payeeBankUser.getTransactions();
		if(!transactions.isEmpty()) {
			for(int x=0;x<transactions.size();x++) {
				Transaction transaction = transactions.get(x);
				if(transaction.getRecipent().equals(user)) {
					transaction.setTransactionAmount(transaction.getTransactionAmount().subtract(amount));
					break;
				}
			}
		}
		payeeBankUser.setCurrentBalance(payeeBankUser.getCurrentBalance().add(amount));
		BankUtil.writeData(payeeBankUser);
	}
}
