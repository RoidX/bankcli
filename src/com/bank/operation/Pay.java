package com.bank.operation;

import java.math.BigDecimal;
import java.util.List;

import com.bank.data.BankUser;
import com.bank.data.Transaction;
import com.bank.util.BankUtil;

public class Pay {
	public BankUser payee;
	public BankUser recipent;
	public BigDecimal amount;
	
	public BigDecimal deductPayee(BankUser payer, BankUser payee, BigDecimal amount) {
		BigDecimal maximumPay = BigDecimal.ZERO;
		List<Transaction> p2transactions = payee.getTransactions();
		boolean hasDeduction = false;
		boolean hasRemoval = false;
		if(!p2transactions.isEmpty()) {
			for(int i=0;i<p2transactions.size();i++) {
				Transaction p2transaction = p2transactions.get(i);
				if(p2transaction.getRecipent().equals(payer.getUsername())) {
					if(p2transaction.getTransactionAmount().compareTo(BigDecimal.ZERO) < 0) {
						maximumPay = amount.compareTo(p2transaction.getTransactionAmount().negate()) > 0 ? p2transaction.getTransactionAmount() : amount ;
						amount = amount.subtract(maximumPay);
						p2transaction.setTransactionAmount(p2transaction.getTransactionAmount().add(maximumPay));
						hasDeduction = true;
						if(p2transaction.getTransactionAmount().compareTo(BigDecimal.ZERO) == 0) {
							p2transactions.remove(i);
							hasRemoval = true;
						}
					}else {
						System.out.println("Returned due to no deduction " + amount);
						return amount;
					}
				}
			}			
		}
		BankUtil.writeData(payee);
		List<Transaction> p1transactions = payer.getTransactions();

		if(!p1transactions.isEmpty()) {
			for(int i=0;i<p1transactions.size();i++) {
				Transaction p1transaction = p1transactions.get(i);
				if(p1transaction.getRecipent().equals(payee.getUsername())) {
					if(hasDeduction) {
						p1transaction.setTransactionAmount(p1transaction.getTransactionAmount().subtract(maximumPay));
					}
					if(hasRemoval) {
						p1transactions.remove(i);
					}
					
				}
			}
		}
		BankUtil.writeData(payer);
		return amount;
	}
	
	public Pay(BankUser payer, BankUser payee, BigDecimal amount) {
		//Before Deduct, check if payee have debts with u
		amount = deductPayee(payer, payee, amount);
		BigDecimal maximumPay = amount.compareTo(payer.getCurrentBalance()) > 0 ? payer.getCurrentBalance() : amount;
		BigDecimal remainingPay = amount.subtract(maximumPay);
		BigDecimal balancePayer = payer.getCurrentBalance().subtract(maximumPay);
		payer.setCurrentBalance(balancePayer);
		if(amount.compareTo(maximumPay) > 0) {
			//Create pending Transaction
			boolean added = false;
			List<Transaction> p1transactions = payer.getTransactions();
			if(p1transactions.isEmpty()) {
				for(int i=0;i<p1transactions.size();i++) {
					Transaction p1transaction = p1transactions.get(i);
					if(p1transaction.getRecipent().equals(payee.getUsername())) {
						BigDecimal balanceTransaction = p1transaction.getTransactionAmount().subtract(remainingPay);
						p1transaction.setTransactionAmount(balanceTransaction);
						added = true;
						break;
					}
				}
			}
			
			if(!added) {
				Transaction p1transaction = new Transaction();
				p1transaction.setRecipent(payee.getUsername());
				p1transaction.setTransactionAmount(remainingPay.negate());
				payer.getTransactions().add(p1transaction);
			}
			
			List<Transaction> p2transactions = payee.getTransactions();
			if(!p2transactions.isEmpty()) {
				for(int i=0;i<p2transactions.size();i++) {
				Transaction p2transaction = p2transactions.get(i);
					if(p2transaction.getRecipent().equals(payer.getUsername())) {
						BigDecimal balanceTransaction = p2transaction.getTransactionAmount().add(remainingPay);
						p2transaction.setTransactionAmount(balanceTransaction);
						System.out.println("Transferred " + remainingPay + " to " + p2transaction.getRecipent());
						
						break;
					}
				}
			}
			if(!added) {
				Transaction p2transaction = new Transaction();
				p2transaction.setRecipent(payer.getUsername());
				p2transaction.setTransactionAmount(remainingPay);
				payee.getTransactions().add(p2transaction);
			}
		}
		BigDecimal balancePayee = payee.getCurrentBalance().add(maximumPay);
		payee.setCurrentBalance(balancePayee);
		BankUtil.writeData(payer);
		BankUtil.writeData(payee);
	}
	
	public BankUser getPayee() {
		return payee;
	}
	public void setPayee(BankUser payee) {
		this.payee = payee;
	}
	public BankUser getRecipent() {
		return recipent;
	}
	public void setRecipent(BankUser recipent) {
		this.recipent = recipent;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
}
