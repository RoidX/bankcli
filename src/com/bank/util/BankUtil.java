package com.bank.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.json.JSONArray;
import org.json.JSONObject;

import com.bank.data.BankUser;
import com.bank.data.Transaction;

public class BankUtil {
	public static void writeData(BankUser user) {
		JSONObject json = new JSONObject();
		File input = new File(user.getUsername() + ".json");
		FileWriter fw = null;
			try {
				if(!input.exists()) {
					input.createNewFile();
				}
				if(input!=null&&input.canWrite()) {
				fw = new FileWriter(input);
				json.put(UserPayloadConstants.USERNAME, user.getUsername());
				json.put(UserPayloadConstants.BALANCE, user.getCurrentBalance());
//				JSONArray transactions = new JSONArray();
//				if(user.getTransactions()!=null) {
//					transactions.put();
//				}
				json.put(UserPayloadConstants.TRANSACTIONS, user.getTransactions());
				fw.write(json.toString());
				fw.flush();
//				System.out.println("Flush");
				}
			}catch(IOException e) {
				e.printStackTrace();
				 
	        } finally {
	 
	            try {
	            	fw.close();
	            	input = null;
//					System.out.println("Close");
	            } catch (IOException e) {
	                // TODO Auto-generated catch block
	                e.printStackTrace();
	            }
	        }
		
	}
	
	public static BankUser loadData(String recipent) {
		if(recipent=="") {
			System.out.println("Please LOGIN first");
			return null;
		}
		JSONObject json = null;
		BankUser bankUser = null;
		File input = new File(recipent + ".json");
		if(input!=null && input.exists() && input.canRead()) {
			try {
				String data = readFile(input.getAbsolutePath(), Charset.defaultCharset());	
				json = new JSONObject(data);
				bankUser = loadUser(json);
			}catch(Exception e) {
				e.printStackTrace();
			}
		}else {
			System.out.println("New User " + recipent);
			//New User
			bankUser = new BankUser();
			bankUser.setUsername(recipent);
			bankUser.setCurrentBalance(BigDecimal.ZERO);
			writeData(bankUser);
		}
		return bankUser;
	}
		
	public static String readFile(String path, Charset encoding) throws IOException{
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}
	
	public static BankUser loadUser(JSONObject json) {
		BankUser user = new BankUser();
//		System.out.println("json.has(UserPayloadConstants.USERNAME)" + json.has(UserPayloadConstants.USERNAME));
//		System.out.println("json.has(UserPayloadConstants.BALANCE)" + json.has(UserPayloadConstants.BALANCE));
//		System.out.println("json.has(UserPayloadConstants.TRANSACTIONS)" + json.has(UserPayloadConstants.TRANSACTIONS));
		
		
		if(json.has(UserPayloadConstants.USERNAME)) {
			user.setUsername(json.getString(UserPayloadConstants.USERNAME));
		}
		if(json.has(UserPayloadConstants.BALANCE)) {
			user.setCurrentBalance(json.getBigDecimal(UserPayloadConstants.BALANCE));
		}
		
		if(json.has(UserPayloadConstants.TRANSACTIONS)) {
			JSONArray transactions = json.getJSONArray(UserPayloadConstants.TRANSACTIONS);
//			System.out.println("transactions.toList().isEmpty()" + transactions.toList().isEmpty());
			if(!transactions.toList().isEmpty()) {
//				System.out.println("transactions.length()" + transactions.length());
				for(int i=0;i<transactions.length();i++) {
				if(transactions.getJSONObject(i)!=null) {
				JSONObject transaction = transactions.getJSONObject(i);
				Transaction transactionObj = new Transaction();
//				System.out.println("transaction.has(UserPayloadConstants.TRANSACTIONS_RECIPENT)" + transaction.has(UserPayloadConstants.TRANSACTIONS_RECIPENT));
//				System.out.println("transaction.has(UserPayloadConstants.TRANSACTIONS_TRX)" + transaction.has(UserPayloadConstants.TRANSACTIONS_TRX));
//				System.out.println("transaction.has(UserPayloadConstants.TRANSACTIONS_FLOAT)" + transaction.has(UserPayloadConstants.TRANSACTIONS_FLOAT));
				
				if(transaction.has(UserPayloadConstants.TRANSACTIONS_RECIPENT)) {
					transactionObj.setRecipent(transaction.getString(UserPayloadConstants.TRANSACTIONS_RECIPENT));
				}
				if(transaction.has(UserPayloadConstants.TRANSACTIONS_TRX)) {
					transactionObj.setTransactionAmount(transaction.getBigDecimal(UserPayloadConstants.TRANSACTIONS_TRX));
				}
				if(transaction.has(UserPayloadConstants.TRANSACTIONS_FLOAT)) {
					transactionObj.setFloatAmount(transaction.getBigDecimal(UserPayloadConstants.TRANSACTIONS_FLOAT));
				}
				user.getTransactions().add(transactionObj);
				}
				}
			}
		}
		return user;
	}
}
