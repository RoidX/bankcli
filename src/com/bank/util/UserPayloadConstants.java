package com.bank.util;

public class UserPayloadConstants {
	public static String USERNAME = "username";
	public static String BALANCE = "balance";
	public static String TRANSACTIONS = "transactions";
	public static String HISTORIES = "histories";
	
	public static String TRANSACTIONS_RECIPENT = "recipent";
	public static String TRANSACTIONS_TRX = "transactionAmount";
	public static String TRANSACTIONS_FLOAT = "floatAmount";
	
}
