package com.bank.data;

import java.math.BigDecimal;

public class Transaction {
	private String recipent;
	private BigDecimal transactionAmount;
	private BigDecimal floatAmount;
	
	public BigDecimal getFloatAmount() {
		return floatAmount;
	}
	public void setFloatAmount(BigDecimal floatAmount) {
		this.floatAmount = floatAmount;
	}
	public String getRecipent() {
		return recipent;
	}
	public void setRecipent(String recipent) {
		this.recipent = recipent;
	}
	public BigDecimal getTransactionAmount() {
		return transactionAmount;
	}
	public void setTransactionAmount(BigDecimal transactionAmount) {
		this.transactionAmount = transactionAmount;
	}
}
