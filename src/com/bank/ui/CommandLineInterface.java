package com.bank.ui;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

import org.json.JSONArray;
import org.json.JSONObject;

import com.bank.data.BankUser;
import com.bank.data.Transaction;
import com.bank.operation.Pay;
import com.bank.operation.Topup;
import com.bank.util.BankUtil;
import com.bank.util.UserPayloadConstants;

public class CommandLineInterface {
	BankUser user;
	
	public static void main(String[] args) {
		new CommandLineInterface();
	}
	
	public CommandLineInterface() {
		Scanner sc= new Scanner(System.in);
		System.out.println("# Bank Command Line Interface");
		String login = "";
		while(sc.hasNext()) {
			String str= sc.nextLine();
			
			System.out.println("\n######" + str);
			String[] split = str.split(" ");
			if(split!=null) {
				String command = split[0].toUpperCase();
				String amount = "";
				BigDecimal amountBD = null;
				String recipent = "";
				switch(command) {
				case "PAY":
					recipent = split[1];
					amount = split[2];
					amountBD = new BigDecimal(amount);	
					new Pay(user, BankUtil.loadData(recipent), amountBD);
					user = BankUtil.loadData(login);
					break;
				case "TOPUP":
					amount = split[1];
					amountBD = new BigDecimal(amount);	
					new Topup(user, amountBD);
					user = BankUtil.loadData(login);
					break;
				case "LOGIN":
					recipent = split[1];
					login = recipent;
					user = BankUtil.loadData(recipent);
					System.out.println("Hello " + login);
					break;
				default:
					System.out.println("INVALID COMMAND");
					break;
				}
				
				if(user!=null) {
					System.out.println("\nYour Balance is " + user.getCurrentBalance());
					List<Transaction> ts = user.getTransactions();
					for(int i=0;i<ts.size();i++) {
						Transaction t = ts.get(i);
						if(t.getTransactionAmount().compareTo(BigDecimal.ZERO) < 0) {
							System.out.println("\nOwing " + t.getTransactionAmount().negate() + " to " + t.getRecipent()) ;
						}else if(t.getTransactionAmount().compareTo(BigDecimal.ZERO) > 0) {
							System.out.println("\n" + t.getRecipent() + " owes you " + t.getTransactionAmount());
						}
						
						
					}
				}
			}
		}
	}
	
}
